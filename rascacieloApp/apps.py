from django.apps import AppConfig


class RascacieloappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rascacieloApp'
