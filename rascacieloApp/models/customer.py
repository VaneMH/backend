from django.db import models


class Customer(models.Model):
    name = models.CharField('Name', max_length = 30)
    address = models.CharField('Address', max_length = 100)
    mobile_phone = models.IntegerField('Mobile Phone', max_length = 30)
    email = models.EmailField('Email', max_length = 100)