from django.db import models


class Item(models.Model):
    name = models.CharField('Name', max_length = 30)
    reference= models.IntegerField('Reference', max_length=10)
    quantity = models.CharField('Quantity', max_length = 10)
    expiration_date = models.TimeField('Expiration Date', max_length=30)