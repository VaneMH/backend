from rascacieloApp.models.item import Item
from rest_framework import serializers
class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ['name', 'reference', 'quantity', 'expiration_date']