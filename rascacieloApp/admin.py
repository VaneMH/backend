from django.contrib import admin
from .models.customer import Customer
from .models.item import Item


admin.site.register(Customer)
admin.site.register(Item)
